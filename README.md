#README#

This README is not complete. 

### What is this repository for? ###

This is the dialog client repo. The client is responsible for:

* Connecting to a dialog server
* Sending messages to a dialog server
* Receiving messages from a dialog server

Version 1.0

### How do I get set up? ###


#### 1.0 Configuration ####

Start by setting up QT on your computer. Reccommend QT 5.6+ 

#### 2.0 Dependencies ####

###### SFML ######

SFML is included in the source of this repo. Simple view the readme.txt document in the SFML folder for installation instructions. 

For exampl, if using Mac, simply open the folder and read the readme.txt file. You will be asked to move the contents of the folder to various places in your system library folder. If you do this, QT should already be configured to load this dependency. 

#### 3.0 Build Client ####

Open the .pro file with QT creator. You will be prompted to create a new user profile for your system so that QT can build for your system. If you are prompted to use an existing user profile, click “no” to create a new one. Depending on the libraries installed on your computer, you might have the option to build this project for Android or IOS in addition to desktop. 

### Running Client and Client Usage ###

#### 1.1 Choosing a Port ####

Once your project builds without error (you may have a lot of annoying warnings), run the project to open a UI. You will be asked to enter a username, port, and server IP address. The defualt port is 9203 and can be manually changed by selecting "other" from the drop down menu. 

After you have hit connect, a new window should open and you should see a message saying that the server has connected.


#### 1.2 Close Client ####

Simple close the window. 

### How to run tests ###

### Deployment instructions ###

### Contribution guidelines ###

#### Writing tests ####

#### Code review ####

#### Other guidelines ####

### Who do I talk to? ###

Josh Buck


 