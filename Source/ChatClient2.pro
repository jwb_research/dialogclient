TEMPLATE = app

QT += qml quick
QT += widgets charts
QT += sql
CONFIG += c++11

SOURCES += main.cpp \
    Client.cpp \
    sqlcontactmodel.cpp \
    sqlconversationmodel.cpp \
    windowlogin.cpp

RESOURCES += \
    Resources/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(Resources/deployment.pri)

HEADERS += \
    Client.h \
    sqlcontactmodel.h \
    sqlconversationmodel.h \
    windowlogin.h

FORMS += \
    windowlogin.ui

DISTFILES += \
    Resources/deployment.pri
