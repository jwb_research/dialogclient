import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Window 2.2

ApplicationWindow {
    visible: true
    width: 540
    height: 960
    title: qsTr("Dialog Client")

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: DialogPage {}
    }
}
