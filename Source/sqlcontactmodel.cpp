#include "sqlcontactmodel.h"

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>

static void createTable()
{
    QSqlDatabase::database().tables().clear();

    if (QSqlDatabase::database().tables().contains(QStringLiteral("Contacts"))) {
        // The table already exists; we don't need to do anything.
        return;
    }

    QSqlQuery query; //CREATE TABLE IF NOT EXISTS
    if (!query.exec(
        "CREATE TABLE IF NOT EXISTS 'Contacts' ("
        "   'name' TEXT NOT NULL,"
        "   PRIMARY KEY(name)"
        ")")) {
        qFatal("Failed to query database: %s", qPrintable(query.lastError().text()));
    }

//    query.exec("INSERT INTO Contacts VALUES('Size, Blend, Cream')");
//    query.exec("INSERT INTO Contacts VALUES('Eggs,Sugar,Cream,Toast')");
//    query.exec("INSERT INTO Contacts VALUES('Credit,Grade,Receipt')");
}

SqlContactModel::SqlContactModel(QObject *parent) :
    QSqlQueryModel(parent)
{
    createTable();

    QObject::connect(Client::Static_Client, SIGNAL(NewDialog(QString&)), this, SLOT(NewContact(QString&)));
    Client::Static_Client->Initialize();

    QSqlQuery query;
    if (!query.exec("SELECT * FROM Contacts"))
        qFatal("Contacts SELECT query failed: %s", qPrintable(query.lastError().text()));

    setQuery(query);
    if (lastError().isValid())
        qFatal("Cannot set query on SqlContactModel: %s", qPrintable(lastError().text()));
}

void SqlContactModel::NewContact(QString & Message) {
    QSqlQuery query;
    std::stringstream ss;
    ss <<  "INSERT INTO Contacts VALUES('" << Message.toStdString() << "')";
    QString q(ss.str().c_str());
    if(!query.exec(q)) {
        qFatal("Failed to query database!");
    }
    QSqlQuery reset;
    if (!reset.exec("SELECT * FROM Contacts"))
        qFatal("Contacts SELECT query failed: %s", qPrintable(reset.lastError().text()));

    setQuery(reset);
    if (lastError().isValid())
        qFatal("Cannot set query on SqlContactModel: %s", qPrintable(lastError().text()));
}
