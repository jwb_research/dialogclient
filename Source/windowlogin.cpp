#include "windowlogin.h"
#include "ui_windowlogin.h"

LoginWindow::LoginWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

LoginWindow::~LoginWindow()
{
    delete ui;
}

void LoginWindow::on_Login_clicked()
{
    // Get Name
    std::string name_string = ui->InputName->text().toStdString();

    // Get Port
    QString PortAsQString;
    if (ui->InputOtherPort->isEnabled()) {
        PortAsQString = ui->InputOtherPort->text();
    }
    else {
        PortAsQString = ui->Port->currentText();
    }
    unsigned short port = (unsigned short)std::atoi(PortAsQString.toStdString().c_str());

    // Get IP
    std::string Address;
    int IP_index = ui->Server_IP->currentIndex();

    if (IP_index == 0) {
        Address = "127.0.0.1";
    }
    else if (IP_index==1) {
        Address = "127.0.0.1";
    }
    else if (IP_index==2) {
        Address = "127.0.0.1";
    }
    else if (IP_index==3) {
        QString IPasString = ui->InputOtherIP->text();
        Address = IPasString.toStdString();
    }

    // Create Client Name
    Client::Static_Client->SetName(name_string);

    // Connect Client
    if (!Client::Static_Client->TCPconnect(Address, port)) {
//        delete Client::Static_Client;
//        Client::Static_Client = NULL;
        ui->Output->setText("Login Failure");
        return;
    }

    // Set client as active
    Client::Static_Client->SetActive();

    // Start Client Window
    this->close(); // Closes current window
}


void LoginWindow::on_Server_IP_currentTextChanged(const QString &arg1)
{
    if (arg1.compare("Other") == 0) {
        ui->InputOtherIP->setEnabled(true);
    }
    else {
        ui->InputOtherIP->setEnabled(false);
    }
}

void LoginWindow::on_Port_currentTextChanged(const QString &arg1)
{
    if (arg1.compare("Other") == 0) {
        ui->InputOtherPort->setEnabled(true);
    }
    else {
        ui->InputOtherPort->setEnabled(false);
    }
}
