/*
FileName:			Client.h




*/

#pragma once
#include <QDebug>
#include <QTcpSocket>
#include <QSqlQuery>

#include <sstream>

typedef quint16 PacketType;
const PacketType INIT_NEW_CLIENT=0;
const PacketType GENERAL_MSG=1;
const PacketType SERVER_MSG=2;
const PacketType COMMAND_MSG=3;
const PacketType PROGRESS_MSG=4;
const PacketType DIALOG_MSG=5;

enum TCP_Status {
    idle = 0,
    Done = 1,
    Disconnected = 2
};

class Client: public QObject
{
    Q_OBJECT
private:

    std::string UserName;
    bool IsActive;
    QTcpSocket Me;

    int CheckMessageType(QString &Message);

public:

    static Client * Static_Client;

    Client();
	~Client();

    void Initialize();

    void SetName(const std::string & UserName) { this->UserName = UserName; }
    bool Active() {return IsActive;}
    void SetActive() {this->IsActive = true;}
    std::string GetName() {return this->UserName;}

    bool TCPconnect(std::string IP, unsigned short port);
    void send(PacketType type, const std::string & msg);

signals:
    void NewMessage(QString &Message);
    void NewDialog(QString &Message);

public slots:
    void ReadMessage();
};

