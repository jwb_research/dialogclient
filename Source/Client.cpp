#include "Client.h"
#include <iostream>
#include <QtNetwork>

Client* Client::Static_Client = new Client();

Client::Client() :
    IsActive(false)
{

}


Client::~Client(void)
{
}


bool Client::TCPconnect(std::string IP, unsigned short port)
{
    // connect to server
    QString IPstr(IP.c_str());
    QHostAddress Address(IPstr);
    quint16 uPort(port);
    Me.connectToHost(Address, uPort);
    if (!Me.waitForConnected(2000)) {
        qDebug() << "Could not connect to server!\n";
        return false;
    }

    connect(&Me, SIGNAL(readyRead()), this, SLOT(ReadMessage()));
    connect(&Me, SIGNAL(disconnected()), this, SLOT(ReadMessage()));

    return true;
}

void Client::send (PacketType type, const std::string & msg)
{
    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out << std::to_string(type).append("_$Token$_").append(msg).c_str();
    if (!Me.write(buffer)) {
        qDebug() << "Error with send!\n";
    }
    Me.waitForBytesWritten();
}

void Client::ReadMessage() {
    QByteArray buffer;
    int dataSize;
    Me.read((char*)&dataSize, sizeof(int));
    buffer = Me.read(dataSize);
    while (buffer.size()) {
        QString Message(buffer);
        buffer.remove(0, Message.length() + 1);
        if (Message.size()) {
            CheckMessageType(Message);
        }
    }
}

int Client::CheckMessageType(QString& Message) {
    QStringList Tokens = Message.split("_$Token$_");
    if (Tokens.size() < 2) {
        qDebug() << "DEBUG: Improper Message: " << Message;
        return -1;
    }

    // TODO: do something when more than 2 tokens

    // Compute Type
    Message = Tokens.at(1);
    QString str = Tokens.at(0);
    str.remove(QRegExp("[^0-9]"));
    int type =  std::stoi(str.toStdString());

    if (type == GENERAL_MSG) {
        std::cout << Message.toStdString() << std::endl;
        emit this->NewMessage(Message);
    }
    else if (type == DIALOG_MSG) {
        std::cout << Message.toStdString() << std::endl;
        emit NewDialog(Message);
    }

    return type;
}

void Client::Initialize() {
    // Send server client name
    this->send(INIT_NEW_CLIENT, this->UserName);
}


