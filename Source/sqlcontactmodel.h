#ifndef SQLCONTACTMODEL_H
#define SQLCONTACTMODEL_H

#include <QSqlQueryModel>
#include "Client.h"

class SqlContactModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    SqlContactModel(QObject *parent = 0);

public slots:
    void NewContact(QString & Message);
};

#endif // SQLCONTACTMODEL_H
